## Logrotate

### Background 

- Ubuntu 20.04 
- nginx 1.16.0
- logs /usr/local/nginx/logs/access/*.log and /usr/local/nginx/logs/error/*.log 
- owner www-data
- group root

### logrorate config

Created file "nginx" in /etc/logrotate.d 

```
/usr/local/nginx/logs/access/*.log
/usr/local/nginx/logs/error/*.log
{
        daily
        missingok
        rotate 14
        compress
        delaycompress
        notifempty
        create 0644 www-data root
        sharedscripts
        postrotate
                [ -s /run/nginx.pid ] && kill -USR1 `cat /run/nginx.pid`
        endscript
}
```

### Update logrorate service

```
sudo vi /lib/systemd/system/logrotate.service
```

Added line. 

```
ReadWritePaths=/usr/local/nginx/logs/access /usr/local/nginx/logs/error
```

```
sudo systemctl daemon-reload
sudo systemctl restart logrotate
```

The service is normally limited to logs in /var/logs.   This adds the nginx folders. 


### Testing 

Check Config 
```
sudo /usr/sbin/logrotate /etc/logrotate.conf --debug
```

Run Logrorate 
```
sudo /usr/sbin/logrotate /etc/logrotate.conf -f
```


